package com.msr;

import com.msr.data.SiteDao;
import com.msr.model.Site;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Respond to site requests
 *
 * @author Measurabl
 * @since 2019-06-06
 */
@RestController
@RequestMapping("/buildings")
public class BuildingsController {

    @Autowired
    private SiteDao siteDao;

    @GetMapping()
    public Iterable<Site> getAll(@RequestParam(required = false) String city) {
        if (city != null) {
            return siteDao.findByCity(city);
        }
        return siteDao.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Site> getSite(@PathVariable int id) {
        return siteDao.findById(id);
    }
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    