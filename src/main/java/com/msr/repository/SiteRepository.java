package com.msr.repository;

import com.msr.model.Site;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Repository functionality for Site
 *
 * @author Measurabl
 * @since 2019-06-06
 */
public interface SiteRepository extends PagingAndSortingRepository<Site, Integer> {

    List<Site> findByCity(String city);
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    