package com.msr.repository;

import com.msr.model.SiteUse;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository functionality for UseTypes
 *
 * @author Measurabl
 * @since 2019-06-12
 */
public interface SiteUseRepository extends JpaRepository<SiteUse, Integer> {

    List<SiteUse> findBySiteId(int siteId);
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    