package com.msr.repository;

import com.msr.model.UseType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository functionality for UseTypes
 *
 * @author Measurabl
 * @since 2019-06-12
 */
public interface UseTypesRepository extends JpaRepository<UseType, Integer> {

}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    