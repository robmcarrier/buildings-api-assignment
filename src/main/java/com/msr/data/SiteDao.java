package com.msr.data;

import com.msr.model.Site;
import com.msr.model.SiteUse;
import com.msr.model.UseType;
import com.msr.repository.SiteRepository;
import com.msr.repository.SiteUseRepository;
import com.msr.repository.UseTypesRepository;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Global DAO functionality not domain driven
 *
 * @author Measurabl
 * @since 2019-06-06
 */
@Service
@Slf4j
public class SiteDao {

    @Autowired
    private SiteRepository siteRepository;

    @Autowired
    private SiteUseRepository siteUseRepository;

    @Autowired
    private UseTypesRepository useTypesRepository;

    /**
     * Returns an iterable of all Sites.
     *
     * @return Iterable of all Sites
     */
    public Iterable<Site> findAll() {
        Iterable<Site> sites = siteRepository.findAll();

        for (Site site : sites) {
            site.setTotalSize(totalSize(site.getId()));
            site.setPrimaryType(getLargestUseType(site.getId()));
        }

        return sites;
    }

    /**
     * Returns Site by Site id.
     *
     * @param siteId Site Id
     * @return Site
     */
    public Optional<Site> findById(int siteId) {
        Optional<Site> site = siteRepository.findById(siteId);

        if (site.isPresent()) {
            Site foundSite = site.get();
            foundSite.setTotalSize(totalSize(siteId));
            foundSite.setPrimaryType(getLargestUseType(siteId));
        }

        return site;
    }

    /**
     * Returns a iterable of Sites that are in a city.
     *
     * @param city City
     * @return Iterable of Sites in specified city
     */
    public Iterable<Site> findByCity(String city) {
        Iterable<Site> sites = siteRepository.findByCity(city);

        for (Site site : sites) {
            site.setTotalSize(totalSize(site.getId()));
            site.setPrimaryType(getLargestUseType(site.getId()));
        }

        return sites;
    }

    /**
     * Returns the total sqft of all Site Uses associated to a Site.
     *
     * @param siteId Site Id
     * @return Total sq ft size of all Site Uses associated to a Site
     */
    private int totalSize(int siteId) {
        Iterable<SiteUse> siteUses = siteUseRepository.findBySiteId(siteId);
        int sum = 0;

        for (SiteUse siteUse : siteUses) {
            sum += siteUse.getSizeSqft();
        }

        return sum;
    }

    /**
     * Returns the largest(by sqft) Use Type associated to a Site.
     *
     * @param siteId Site Id
     * @return Returns the largest(by sqft) Use Type associated to a Site
     */
    private Optional<UseType> getLargestUseType(int siteId) {
        Iterable<SiteUse> siteUses = siteUseRepository.findBySiteId(siteId);
        long highestSqFt = 0;
        int highestSiteUseId = 0;

        for (SiteUse siteUse : siteUses) {
            if (highestSqFt < siteUse.getSizeSqft()) {
                highestSqFt = siteUse.getSizeSqft();
                highestSiteUseId = siteUse.getUseTypeId();
            }
        }

        return useTypesRepository.findById(highestSiteUseId);
    }

}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    