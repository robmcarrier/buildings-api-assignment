package com.msr.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Data;

/**
 * Site uses POJO
 *
 * @author Measurabl
 * @since 2019-06-11
 */
@Data
@Entity
public class SiteUse {

    @Id
    private int id;

    private int siteId;

    private String description;

    private long sizeSqft;

    private int useTypeId;
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    