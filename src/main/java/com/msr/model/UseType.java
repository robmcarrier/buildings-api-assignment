package com.msr.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Data;

/**
 * Lookup types
 *
 * @author Measurabl
 * @since 2019-06-11
 */
@Data
@Entity
public class UseType {

    @Id
    private int id;

    private String name;
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    